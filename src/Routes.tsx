import { lazy, Suspense } from "react"
import { Route, Switch } from "react-router-dom"
//icons
import { CountryIcon, OrganizationIcon } from "ftl-dashboards-ui-kit"
//Templates
import { FTLErrorPage, FTLBaseLayout } from "ftl-dashboards-templates"

import { FTLLoader } from "ftl-dashboards-ui-kit"
import {
  CountryAPI,
  OrganizationAPI,
  PictureAPI,
  ProfileAPI,
  UtilsAPI,
} from "./repository"
import { PrivateRoute } from "ftl-auth-module"

//Profile
const ProfilePage = lazy(
  () => import("ftl-dashboards-templates/dist/templates/FTLProfilePage")
)

//Auth
const AuthPage = lazy(
  () => import("ftl-dashboards-templates/dist/templates/FTLAuthPage")
)

//Countries
const CountriesList = lazy(() => import("./pages/Countries/List"))
const CountriesDetailPage = lazy(() => import("./pages/Countries/Detail"))

//Organizations
const OrganizationsList = lazy(() => import("./pages/Organizations/List"))
const OrganizationDetalPage = lazy(() => import("./pages/Organizations/Detail"))

export const Routes = () => {
  return (
    <Suspense fallback={<FTLLoader height="50%" color="dark" />}>
      <Switch>
        <Route exact path="/">
          <AuthPage
            redirectOnAuthUrl="/countries"
            title="Войти в Marketing Admin"
          />
        </Route>
        <Route exact path="/access-error">
          <FTLErrorPage hasGoBackButton={false} fullScreen />
        </Route>
        <Route exact path="/user-blocked">
          <FTLErrorPage
            title="Аккаунт заблокирован"
            message="Обратитесь к администратору"
            hasGoBackButton={false}
            fullScreen
          />
        </Route>
        <Route>
          <FTLBaseLayout
            title="Example Admin"
            items={[
              {
                icon: <CountryIcon />,
                label: "Страны",
                to: "/countries/",
              },
              {
                icon: <OrganizationIcon />,
                label: "Огранизации",
                to: "/organizations/",
              },
            ]}
          >
            <Switch>
              <PrivateRoute exact path="/profile">
                <ProfilePage
                  getProfile={ProfileAPI.get}
                  editProfile={ProfileAPI.edit}
                  getCountry={CountryAPI.getById}
                  getCountries={CountryAPI.getList}
                  getDefaultCountry={() => UtilsAPI.getDefault("countries")}
                  deleteProfilePicture={ProfileAPI.deletePicture}
                  changeProfilePassword={ProfileAPI.changePassword}
                  postPicture={PictureAPI.post}
                />
              </PrivateRoute>

              <PrivateRoute exact path="/countries/">
                <CountriesList />
              </PrivateRoute>
              <PrivateRoute exact path="/countries/new">
                <CountriesDetailPage
                  getCountry={CountryAPI.getById}
                  createCountry={CountryAPI.create}
                  pageType="new"
                />
              </PrivateRoute>
              <PrivateRoute exact path="/countries/:id">
                <CountriesDetailPage
                  getCountry={CountryAPI.getById}
                  editCountry={CountryAPI.edit}
                  pageType="edit"
                />
              </PrivateRoute>

              <PrivateRoute exact path="/organizations/">
                <OrganizationsList />
              </PrivateRoute>
              <PrivateRoute exact path="/organizations/new">
                <OrganizationDetalPage
                  getOrganization={OrganizationAPI.getById}
                  createOrganization={OrganizationAPI.create}
                  pageType="new"
                />
              </PrivateRoute>
              <PrivateRoute exact path="/organizations/:id">
                <OrganizationDetalPage
                  pageType="edit"
                />
              </PrivateRoute>

              <PrivateRoute exact path="/error">
                <FTLErrorPage />
              </PrivateRoute>

              <PrivateRoute exact path="/permission-denied">
                <FTLErrorPage
                  title="Доступ запрещён"
                  message="Недостаточно прав для доступа. Обратитесь к администратору сервиса
                        или вернитесь в предыдущий раздел"
                />
              </PrivateRoute>
              <PrivateRoute exact path={["*", "/404"]}>
                <FTLErrorPage
                  title="Страницы не существует"
                  message="Наверное, вы перешли по неправильной ссылке."
                />
              </PrivateRoute>
            </Switch>
          </FTLBaseLayout>
        </Route>
      </Switch>
    </Suspense>
  )
}
