import { lazy } from "react"
import {
  snackbarService,
  handleError,
  useDetailPage,
  useTabs,
  Country,
} from "ftl-dashboards-core"
import FTLDetailPage from "ftl-dashboards-templates/dist/templates/FTLDetailPage"
import { FTLTab, FTLTabs, useLayout } from "ftl-dashboards-ui-kit"
import { initState, countrySchema } from "./model"

const GeneralTab = lazy(() => import("./layouts/GeneralTab"))
const PhoneTab = lazy(() => import("./layouts/PhoneTab"))

const CountriesDetail = ({
  getCountry,
  createCountry,
  editCountry,
  pageType,
}: any) => {
  const { tabIndex, setTabIndex, goToNextTab, goToPrevTab } = useTabs()

  const {
    id,
    formik,
    isFormNotChanged,
    history,
    setEntityData,
    setIsFetching,
  } = useDetailPage({
    pageType: "edit",
    initialValues: initState,
    enableReinitialize: true,
    validationSchema: countrySchema,
    defaultError:
      "Произошла ошибка при получении данных аккаунта push сервиса!",
    getDetails: getCountry,
    handleFetchedEntity: async (country) => {
      if (country) {
        let data = {
          ...formik.values,
          ...country.data.result,
        }
        try {
          formik.setValues(data, false)
          setEntityData(data)
        } catch (error) {
          handleError({
            error,
            defaultError: "Произошла ошибка при получении данных",
          })
        } finally {
          setIsFetching(false)
        }
      }
    },
    onSubmit: async (values) => {
      try {
        switch (pageType) {
          case "new":
            await createCountry(values)
            formik.setValues(
              {
                ...formik.values,
              },
              false
            )
            snackbarService.showSnackbar({
              variant: "success",
              message: "Страна успешно создана",
            })
            break
          case "edit":
            if (id) {
              await editCountry(values as Country<"PATCH">)

              snackbarService.showSnackbar({
                variant: "success",
                message: "Параметры страны успешно обновлены",
              })
            }
            break
        }
      } catch (error) {
        handleError({
          error,
          defaultError: "Произошла ошибка при отправке данных",
        })
      } finally {
        history.push("/countries")
        formik.setSubmitting(false)
      }
    },
  })

  const page = useLayout({
    components: {
      Breadcrumbs: {
        items: [
          {
            label: "Страны",
            to: "/countries",
          },
          {
            label: pageType === "edit" ? "Параметры страны" : "Новая страна",
            to: "#",
          },
        ],
      },
      Header: {
        title: pageType === "edit" ? "Параметры страны" : "Новая страна",
      },
      Toolbar: {
        onSaveBtnId: "form",
        async: formik.isSubmitting,
        disabled: isFormNotChanged,
        onSaveMessage: "Сохранить",
        onCancel: () => history.push("/countries"),
        currentTabIndex: tabIndex,
        length: 1,
        goToNextTab,
        goToPrevTab,
      },
    },
  })
  return (
    <FTLDetailPage
      formik={formik}
      headerComponent={
        <>
          {page.Breadcrumbs}
          {page.Header}
        </>
      }
      toolbarComponent={page.Toolbar}
    >
      <FTLTabs
        value={tabIndex}
        onChange={(e, newValue) => {
          setTabIndex(newValue)
        }}
      >
        <FTLTab label="Общие данные" />
        <FTLTab label="Формат номера" />
      </FTLTabs>
      <form
        noValidate
        id="form"
        onSubmit={(e) => {
          e.preventDefault()
          formik.handleSubmit()
          if (formik.errors.name || formik.errors.activeStatus) setTabIndex(0)
          else if (
            formik.errors.phoneNumberFormat?.code ||
            formik.errors.phoneNumberFormat?.fullMask ||
            formik.errors.phoneNumberFormat?.mask ||
            formik.errors.phoneNumberFormat?.allowedFirstDigits
          )
            setTabIndex(1)
        }}
      >
        {tabIndex === 0 && <GeneralTab formik={formik} />}
        {tabIndex === 1 && <PhoneTab formik={formik} />}
      </form>
    </FTLDetailPage>
  )
}

export default CountriesDetail
