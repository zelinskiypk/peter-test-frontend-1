import { Box } from "@material-ui/core"
import { FTLTextField, FTLSelect } from "ftl-dashboards-ui-kit"
import { statusItems } from "../model"
import { GeneralTabProps } from "../types"

export const GeneralTab = ({ formik }: GeneralTabProps) => {
  return (
    <Box width="360px">
      <Box mt={6}>
        <FTLTextField
          required
          name="name"
          label="Наименование"
          value={formik.values.name}
          error={
            formik.errors.name && formik.submitCount > 0 && formik.touched.name
              ? true
              : false
          }
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          fullWidth
        />
      </Box>
      <Box mt={6}>
        <FTLSelect
          required
          label="Активность"
          isClearable
          value={statusItems.find((item) => {
            if (item.value === formik.values.activeStatus) return true
          })}
          onBlur={formik.handleBlur}
          options={statusItems}
          onChange={(option) => {
            formik.setFieldValue(
              "activeStatus",
              (option as { value: string })?.value
            )
          }}
          isError={
            formik.errors.activeStatus && formik.submitCount > 0 ? true : false
          }
          BoxProps={{
            maxWidth: 200,
          }}
          defaultValue={
            formik.values.activeStatus
              ? statusItems.find((item) => {
                  if (item.value === formik.values.activeStatus) return true
                })
              : statusItems[0]
          }
        />
      </Box>
    </Box>
  )
}

export default GeneralTab
