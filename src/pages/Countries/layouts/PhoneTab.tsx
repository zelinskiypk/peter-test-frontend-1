import { Box, makeStyles, Typography } from "@material-ui/core"
import { FTLTextField, FTLSelect } from "ftl-dashboards-ui-kit"
import { GeneralTabProps } from "../types"

const firstDigits = [
  { label: "1", value: "1" },
  { label: "2", value: "2" },
  { label: "3", value: "3" },
  { label: "4", value: "4" },
  { label: "5", value: "5" },
  { label: "6", value: "6" },
  { label: "7", value: "7" },
  { label: "8", value: "8" },
  { label: "9", value: "9" },
]

export const PhoneTab = ({ formik }: GeneralTabProps) => {
  const classes = useStyles()

  return (
    <Box width="360px" display="grid" gridGap="24px">
      <Box mt={6} display="flex" flexWrap="wrap">
        <Box width="80px">
          <FTLTextField
            required
            label="Код страны"
            name="phoneNumberFormat.code"
            value={formik.values.phoneNumberFormat.code}
            error={
              formik.errors?.phoneNumberFormat?.code &&
              formik.submitCount > 0 &&
              formik.touched.phoneNumberFormat?.code
                ? true
                : false
            }
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            fullWidth
          />
        </Box>
        <Box ml={2} width="calc(100% - 88px)">
          <FTLTextField
            fullWidth
            label="Маска номера (без кода страны)"
            name="phoneNumberFormat.mask"
            required
            value={formik.values.phoneNumberFormat.mask}
            error={
              formik.errors?.phoneNumberFormat?.mask &&
              formik.submitCount > 0 &&
              formik.touched.phoneNumberFormat?.mask
                ? true
                : false
            }
            placeholder="(###) ### ## ##"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </Box>
        <Typography className={classes.item} variant="h5">
          Допустимые символы 0-9,(),#, +
        </Typography>
      </Box>
      <Box>
        <FTLTextField
          label="Полная маска номера"
          required
          name="phoneNumberFormat.fullMask"
          fullWidth
          placeholder="+7 (###) ### ## ##"
          // value={formik.values.phoneNumberFormat.fullMask.replace(/[A-z]/g, '')}
          value={formik.values.phoneNumberFormat.fullMask}
          error={
            formik.errors?.phoneNumberFormat?.fullMask &&
            formik.submitCount > 0 &&
            formik.touched.phoneNumberFormat?.fullMask
              ? true
              : false
          }
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
        />
        <Typography className={classes.item} variant="h5">
          Допустимые символы 0-9,(),#, +
        </Typography>
      </Box>
      <FTLSelect
        label="Допустимые первые символы номера"
        required
        isClearable
        withCheckboxes
        value={formik.values.phoneNumberFormat.allowedFirstDigits?.map(
          (item: number) => ({
            value: String(item),
            label: String(item),
          })
        )}
        isMulti
        onBlur={formik.handleBlur}
        options={firstDigits}
        onChange={(option: any) => {
          formik.setFieldValue(
            "phoneNumberFormat.allowedFirstDigits",
            option?.map((el: any) => Number(el.value))
          )
        }}
        isError={
          formik.errors?.phoneNumberFormat?.allowedFirstDigits &&
          formik.submitCount > 0
            ? true
            : false
        }
      />
    </Box>
  )
}
const useStyles = makeStyles({
  item: {
    color: "#627084",
    fontSize: 12,
    lineHeight: "16px",
    marginTop: 4,
    fontFamily: "Inter",
    fontStyle: "normal",
    fontWeight: 400,
    letterSpacing: "0.02px",
  },
})

export default PhoneTab
