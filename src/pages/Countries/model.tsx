import * as Yup from "yup"
import {
  ActivityItem,
  ActivityKeys,
  Country,
  CountryRequest,
  Denormalized,
  dialogService,
  FTLDateUtils,
  GetListResponse,
  handleError,
  snackbarService,
} from "ftl-dashboards-core"
import { Dictionary } from "ftl-dashboards-core"
import { Box } from "@material-ui/core"
import { EditIcon, FTLIconButton, LockIcon } from "ftl-dashboards-ui-kit"
import { CountryAPI, UtilsAPI } from "../../repository"

export const statusLabels: Dictionary<string> = {
  ACTIVE: "Активна",
  ARCHIVE: "Приостановлена",
}

export const statusItems: Array<ActivityItem> = Object.keys(statusLabels).map(
  (key) => {
    return {
      value: key as ActivityKeys,
      label: statusLabels[key],
    }
  }
)

//List

export type CountryContextProps = {
  setIsFetching: React.Dispatch<React.SetStateAction<boolean>>
  setPageCount: React.Dispatch<React.SetStateAction<number>>
  setData: React.Dispatch<React.SetStateAction<Country[]>>
}

export const onSubmitCountryDialog = async (
  id: string,
  request: CountryRequest,
  context: CountryContextProps
) => {
  context.setIsFetching(true)
  try {
    await UtilsAPI.switchActiveStatus("countries", id)
    await getCountries(request, context)
    snackbarService.showSnackbar({
      variant: "success",
      message: "Активность страны успешно изменена",
    })
  } catch (error) {
    handleError({
      error,
      defaultError: "Произошла ошибка при изменении активности",
    })
  } finally {
    context.setIsFetching(false)
  }
}

export const normalizeCountries = (
  { items, count }: Denormalized<Country<"GET">>,
  request: CountryRequest,
  context: CountryContextProps
) => {
  const normalizeDataOfTable: Country[] = items.map((item: Country) => {
    const active = item.activeStatus === "ACTIVE" ? true : false
    return {
      ...item,
      activeStatus: statusLabels[item.activeStatus] as ActivityKeys,
      createdAt: FTLDateUtils.format(new Date(item?.createdAt ?? ""), {
        breakHours: false,
      }),
      action: (
        <Box display="flex" justifyContent="center" alignItems="center">
          <FTLIconButton
            onClick={(e) => {
              e.stopPropagation() //это нужно, чтобы не сработал клик по строке (всплытие прерывается)
              dialogService.showDialog({
                onSubmit: () => {
                  item.id && onSubmitCountryDialog(item.id, request, context)
                  dialogService.closeDialog()
                },
                children: active
                  ? "Вы действительно хотите приостановить возможность выбора данной страны при взаимодействии с системой?"
                  : "Вы действительно хотите возобновить возможность выбора данной страны  при взаимодействии с системой?",
              })
            }}
          >
            <LockIcon open={item.activeStatus === "ACTIVE"} />
          </FTLIconButton>
          <FTLIconButton>
            <EditIcon />
          </FTLIconButton>
        </Box>
      ),
    }
  })
  context.setPageCount(count)
  context.setData(normalizeDataOfTable)
  return normalizeDataOfTable
}

export const getCountries = async (
  request: CountryRequest,
  context: CountryContextProps
) => {
  context.setIsFetching(true)
  try {
    const countries = (await CountryAPI.getList(request)).data.result
    normalizeCountries(countries, request, context)
  } catch (error) {
    handleError({
      error,
      defaultError: "Произошла ошибка при получении данных стран",
    })
  } finally {
    context.setIsFetching(false)
  }
}

//Detail
export const initState: Country = {
  id: "",
  name: "",
  activeStatus: "ACTIVE",
  phoneNumberFormat: {
    allowedFirstDigits: [],
    code: "",
    fullMask: "",
    mask: "",
    digitsQuantity: 10,
    digitsQuantityWithCode: 10,
  },
}

export const countrySchema = Yup.object({
  name: Yup.string().required(),
  activeStatus: Yup.string().required(),
  phoneNumberFormat: Yup.object({
    allowedFirstDigits: Yup.array().of(Yup.string()),
    code: Yup.string().required(),
    fullMask: Yup.string()
      .matches(/^[0-9+\(\)#]{5,20}$/, {
        message: "Некорректный формат номера",
        // excludeEmptyString: true,
      })
      .required(),
    mask: Yup.string()
      .matches(/^[0-9+\(\)#]{5,20}$/, {
        message: "Некорректный формат номера",
        // excludeEmptyString: true,
      })
      .required(),
  }),
})
