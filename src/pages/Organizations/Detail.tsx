import { lazy } from "react"
import {
  snackbarService,
  handleError,
  useDetailPage,
  useTabs,
} from "ftl-dashboards-core"
import FTLDetailPage from "ftl-dashboards-templates/dist/templates/FTLDetailPage"
import { useLayout, FTLTextField, FTLSelect } from "ftl-dashboards-ui-kit"
import { initState, organizationSchema } from "./model"
import { Organization } from "../../types/organization"
import { Box } from "@material-ui/core"
import { statusItems } from "./model"

const OrganizationDetail = ({
  getOrganization,
  createOrganization,
  editOrganization,
  pageType,
}: any) => {
  const { tabIndex, setTabIndex, goToNextTab, goToPrevTab } = useTabs()

  const {
    id,
    formik,
    isFormNotChanged,
    history,
    setEntityData,
    setIsFetching,
  } = useDetailPage({
    pageType: "edit",
    initialValues: initState,
    enableReinitialize: true,
    validationSchema: organizationSchema,
    defaultError:
      "Произошла ошибка при получении данных аккаунта push сервиса!",
    getDetails: getOrganization,
    handleFetchedEntity: async (organization) => {
      if (organization) {
        let data = {
          ...formik.values,
          ...organization.data.result,
        }
        try {
          formik.setValues(data, false)
          setEntityData(data)
        } catch (error) {
          handleError({
            error,
            defaultError: "Произошла ошибка при получении данных",
          })
        } finally {
          setIsFetching(false)
        }
      }
    },
    onSubmit: async (values) => {
      try {
        switch (pageType) {
          case "new":
            await createOrganization(values)
            formik.setValues(
              {
                ...formik.values,
              },
              false
            )
            snackbarService.showSnackbar({
              variant: "success",
              message: "Организация успешно создана",
            })
            break
          case "edit":
            if (id) {
              await editOrganization(values as Organization<"PATCH">)

              snackbarService.showSnackbar({
                variant: "success",
                message: "Параметры организации успешно обновлены",
              })
            }
            break
        }
      } catch (error) {
        handleError({
          error,
          defaultError: "Произошла ошибка при отправке данных",
        })
      } finally {
        history.push("/organizations")
        formik.setSubmitting(false)
      }
    },
  })

  const page = useLayout({
    components: {
      Breadcrumbs: {
        items: [
          {
            label: "Организации",
            to: "/organizations",
          },
          {
            label:
              pageType === "edit"
                ? "Параметры организации"
                : "Новая организация",
            to: "#",
          },
        ],
      },
      Header: {
        title:
          pageType === "edit" ? "Параметры организации" : "Новая организация",
      },
      Toolbar: {
        onSaveBtnId: "form",
        async: formik.isSubmitting,
        disabled: isFormNotChanged,
        onSaveMessage: "Сохранить",
        onCancel: () => history.push("/organizations"),
        currentTabIndex: tabIndex,
        length: 1,
        goToNextTab,
        goToPrevTab,
      },
    },
  })
  return (
    <FTLDetailPage
      formik={formik}
      headerComponent={
        <>
          {page.Breadcrumbs}
          {page.Header}
        </>
      }
      toolbarComponent={page.Toolbar}
    >
      <form
        noValidate
        id="form"
        onSubmit={(e) => {
          e.preventDefault()
          formik.handleSubmit()
          setTabIndex(0)
        }}
      >
        <Box width="360px">
          <Box mt={6}>
            <FTLTextField
              required
              name="name"
              label="Наименование"
              value={formik.values.name}
              error={
                formik.errors.name &&
                formik.submitCount > 0 &&
                formik.touched.name
                  ? true
                  : false
              }
              onChange={formik.handleChange}
              onBlur={formik.handleBlur}
              fullWidth
            />
          </Box>
          <Box mt={6}>
            <FTLSelect
              required
              label="Активность"
              isClearable
              value={statusItems.find((item) => {
                if (item.value === formik.values.activeStatus) return true
              })}
              onBlur={formik.handleBlur}
              options={statusItems}
              onChange={(option) => {
                formik.setFieldValue(
                  "activeStatus",
                  (option as { value: string })?.value
                )
              }}
              isError={
                formik.errors.activeStatus && formik.submitCount > 0
                  ? true
                  : false
              }
              BoxProps={{
                maxWidth: 200,
              }}
              defaultValue={
                formik.values.activeStatus
                  ? statusItems.find((item) => {
                      if (item.value === formik.values.activeStatus) return true
                    })
                  : statusItems[0]
              }
            />
          </Box>
        </Box>
      </form>
    </FTLDetailPage>
  )
}

export default OrganizationDetail
