import { useState, useCallback, useEffect } from "react"
import { FTLStatusTag, useLayout } from "ftl-dashboards-ui-kit"
import { getOrganizations, statusItems, statusLabels } from "./model"
import FTLListPage from "ftl-dashboards-templates/dist/templates/FTLListPage"
import { useListPage, FTLObjectUtils } from "ftl-dashboards-core"
import { Organization, OrganizationRequest } from "../../types/organization"

export const columns = [
  {
    Header: "Наименование",
    accessor: "name",
    width: 650,
    disableSortBy: true,
  },
  {
    Header: "Активность",
    width: 150,
    accessor: "activeStatus",
    disableSortBy: true,
    Cell: (props: any) => {
      const value = props.cell.value
      return (
        <FTLStatusTag
          status={value === "Активна" ? "success" : "error"}
          style={{
            display: "inline-block",
          }}
        >
          {value}
        </FTLStatusTag>
      )
    },
  },
  {
    Header: "Создан",
    accessor: "createdAt",
    width: 180,
    align: "right",
  },
  {
    Header: "",
    accessor: "action",
    width: 80,
    align: "right",
    disableSortBy: true,
  },
]

const OrganizationsList = () => {
  const [data, setData] = useState<Organization[]>([])

  const {
    pageCount,
    isFetching,
    query,
    debouncedQuery,
    history,
    sortName,
    sortDirection,
    limit,
    offset,
    filters,
    setFilters,
    setPageCount,
    setQuery,
    setIsFetching,
  } = useListPage()

  const fetchOrganizations = useCallback(
    async (request: OrganizationRequest) =>
      getOrganizations(request, {
        setIsFetching,
        setData,
        setPageCount,
      }),
    []
  )

  useEffect(() => {
    fetchOrganizations({
      query: debouncedQuery,
      activeStatus: filters.activeStatus?.value,
      sortName,
      sortDirection,
      limit,
      offset,
    })
  }, [filters, sortName, sortDirection])

  const page = useLayout({
    components: {
      Header: {
        title: "Организации",
        primaryButton: {
          label: "Новая организация",
          onClick: () => history.push("/organizations/new"),
        },
      },
      Table: {
        data,
        columns,
        pageCount,
        isFetching,
        setIsFetching,
        actionsSize: 2,
        isSearching: Boolean(FTLObjectUtils.getNotEmptyFieldsCount(filters)),
        onFetchData: (args) =>
          fetchOrganizations({
            query,
            activeStatus: filters.activeStatus?.value,
            sortName,
            sortDirection,
            ...args,
          }),
        rowDisable: (row) => {
          if (row.original.activeStatus === statusLabels["ARCHIVE"]) return true
          return false
        },
        onRowClick: (row) => {
          history.push(`/organizations/${row.original.id}`)
        },
      },
    },
  })
  return (
    <FTLListPage
      headerComponent={page.Header}
      filters={[
        {
          isSearchable: false,
          options: statusItems,
          onChange: (option) => {
            setFilters({ activeStatus: option })
          },
          value: filters.activeStatus,
          placeholder: "Активность",
        },
      ]}
      searchProps={{
        value: query,
        onChange: (value) => setQuery(value),
        placeholder: "Наименование",
      }}
      tableComponent={page.Table}
    />
  )
}
export default OrganizationsList
