import * as Yup from "yup"
import {
  ActivityItem,
  ActivityKeys,
  Denormalized,
  dialogService,
  FTLDateUtils,
  GetListResponse,
  handleError,
  snackbarService,
} from "ftl-dashboards-core"
import { Dictionary } from "ftl-dashboards-core"
import { Box } from "@material-ui/core"
import { EditIcon, FTLIconButton, LockIcon } from "ftl-dashboards-ui-kit"
import { OrganizationAPI, UtilsAPI } from "../../repository"
import { Organization, OrganizationRequest } from "../../types/organization"

export const statusLabels: Dictionary<string> = {
  ACTIVE: "Активна",
  ARCHIVE: "Приостановлена",
}

export const statusItems: Array<ActivityItem> = Object.keys(statusLabels).map(
  (key) => {
    return {
      value: key as ActivityKeys,
      label: statusLabels[key],
    }
  }
)

//List

export type OrganizationContextProps = {
  setIsFetching: React.Dispatch<React.SetStateAction<boolean>>
  setPageCount: React.Dispatch<React.SetStateAction<number>>
  setData: React.Dispatch<React.SetStateAction<Organization[]>>
}

export const onSubmitOrganizationDialog = async (
  id: string,
  request: OrganizationRequest,
  context: OrganizationContextProps
) => {
  context.setIsFetching(true)
  try {
    await UtilsAPI.switchActiveStatus("organizations", id)
    await getOrganizations(request, context)
    snackbarService.showSnackbar({
      variant: "success",
      message: "Активность организации успешно изменена",
    })
  } catch (error) {
    handleError({
      error,
      defaultError: "Произошла ошибка при изменении активности",
    })
  } finally {
    context.setIsFetching(false)
  }
}

export const normalizeOrganizations = (
  { items, count }: Denormalized<Organization<"GET">>,
  request: OrganizationRequest,
  context: OrganizationContextProps
) => {
  const normalizeDataOfTable: Organization[] = items.map(
    (item: Organization) => {
      const active = item.activeStatus === "ACTIVE" ? true : false
      return {
        ...item,
        activeStatus: statusLabels[item.activeStatus] as ActivityKeys,
        createdAt: FTLDateUtils.format(new Date(item?.createdAt ?? ""), {
          breakHours: false,
        }),
        action: (
          <Box display="flex" justifyContent="center" alignItems="center">
            <FTLIconButton
              onClick={(e) => {
                e.stopPropagation() //это нужно, чтобы не сработал клик по строке (всплытие прерывается)
                dialogService.showDialog({
                  onSubmit: () => {
                    item.id &&
                      onSubmitOrganizationDialog(item.id, request, context)
                    dialogService.closeDialog()
                  },
                  children: active
                    ? "Вы действительно хотите приостановить возможность выбора данной организации при взаимодействии с системой?"
                    : "Вы действительно хотите возобновить возможность выбора данной организации при взаимодействии с системой?",
                })
              }}
            >
              <LockIcon open={item.activeStatus === "ACTIVE"} />
            </FTLIconButton>
            <FTLIconButton>
              <EditIcon />
            </FTLIconButton>
          </Box>
        ),
      }
    }
  )
  context.setPageCount(count)
  context.setData(normalizeDataOfTable)
  return normalizeDataOfTable
}

export const getOrganizations = async (
  request: OrganizationRequest,
  context: OrganizationContextProps
) => {
  context.setIsFetching(true)
  try {
    const countries = (await OrganizationAPI.getList(request)).data.result
    normalizeOrganizations(countries, request, context)
  } catch (error) {
    handleError({
      error,
      defaultError: "Произошла ошибка при получении данных организаций",
    })
  } finally {
    context.setIsFetching(false)
  }
}

//Detail
export const initState: Organization = {
  id: "",
  name: "",
  activeStatus: "ACTIVE",
}

export const organizationSchema = Yup.object({
  name: Yup.string().required(),
  activeStatus: Yup.string().required(),
})
