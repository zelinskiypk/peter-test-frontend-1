import { FormikProps } from "formik"
import { Request } from "../types"
import { BasicEntity } from "../basicEntity/types"
import { ActivityKeys } from "../../types//activity"
import { PhoneNumberFormat } from "../../types//phoneNumberFormat"
import { Organization } from "../../types/organization"

export type GeneralTabProps = {
  formik: FormikProps<Organization>
}
