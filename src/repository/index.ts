// import { createEffect, attach, createStore } from "effector"
import { AxiosResponse } from "axios"

//types
import {
  createUtilsRepository,
  createRepositoryInstance,
  createProfileRepository,
  createCountryRepository,
  createEntityRepository,
  CountryRequest,
} from "ftl-dashboards-core"
import { Organization, OrganizationRequest } from "../types/organization"

enum Versions {
  v1 = "v1",
  v2 = "v2",
}

export const baseUrl =
  process.env.REACT_APP_ENV === "dev"
    ? `https://ftl-tech-admin-api-stg1.sushivesla.net/api/${Versions.v1}`
    : `https://tech-admin-api.prod-1.foodtech-lab.ru/api/${Versions.v1}`
    
export const apiBaseUrl =
  process.env.REACT_APP_ENV === "dev"
    ? "https://ftl-tech-admin-api-stg1.sushivesla.net"
    : "https://tech-admin-api.prod-1.foodtech-lab.ru"

export const repository = createRepositoryInstance({
  baseUrl,
  apiBaseUrl,
  endPoints: {
    userBlocked: "/user-blocked",
    authPage: "/",
  },
})

export const UtilsAPI = createUtilsRepository(repository)

//Profile

export const ProfileAPI = createProfileRepository(repository)

//Country

export const CountryAPI = createCountryRepository(repository, UtilsAPI)

//Picture

export const PictureAPI = Object.freeze({
  post: (request: FormData): Promise<AxiosResponse<{ result: string }>> => {
    return repository.post(`pictures`, request)
  },
})

//Organizations
const normalizeOrganizationPayload = (
  payload: Organization<"POST"> | Organization<"PATCH">
): Organization => ({
  activeStatus: payload.activeStatus,
  name: payload.name,
})

export const OrganizationAPI = createEntityRepository<
  OrganizationRequest,
  Organization<"GET">
>({
  repositoryInstance: repository,
  path: "organizations",
  utils: UtilsAPI,
  normalizeEntityPayload: normalizeOrganizationPayload,
})
