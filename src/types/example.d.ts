import {
  BasicEntity,
  BasicEntityTypes,
  Request,
  SelectOption,
} from "ftl-dashboards-core"

export type ExampleEntityRequest = Request & {
  activeStatus?: string
  organizationId?: string
}

export type ExampleEntityFilters = {
  activeStatus?: SelectOption | null
  organization?: SelectOption | null
}
