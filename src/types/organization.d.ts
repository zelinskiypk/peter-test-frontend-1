import {
  ActivityKeys,
  BasicEntity,
  BasicEntityTypes,
  Request,
} from "ftl-dashboards-core"

export type Organization<
  EntityType extends BasicEntityTypes = "POST"
> = BasicEntity<EntityType> & {
  name: string
  activeStatus: ActivityKeys
}

export type OrganizationRequest = Request & {
  isDefault?: boolean
  activeStatus?: ActivityKeys
}
